package com.teamcheaters.thecheatgame;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class welcome extends AppCompatActivity {

    Typeface font_later;
    TextView tv_game_title;
    Button bt_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        font_later = Typeface.createFromAsset(getAssets(), "fonts/28 Days Later.ttf");
        tv_game_title = (TextView)findViewById(R.id.gameTitle);
        assert tv_game_title != null;
        tv_game_title.setTypeface(font_later);

        bt_login = (Button)findViewById(R.id.login_bt);
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(welcome.this, login.class));
            }
        });

    }
}
